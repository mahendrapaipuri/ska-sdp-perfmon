""""This package contains functions to export metric data in HDF5 format"""

import os
import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def hdf5_exporter(config, df_dict):
    """This method exports the dataframe data into HDF5 data store"""

    # Initialise store
    for metric, df in df_dict.items():
        if df.empty:
            _log.debug('No %s data found to export to excel. Skipping' % metric)
            continue
        file_name = os.path.join(config['save_dir'], ".".join([metric, 'h5']))
        df.to_hdf(
            file_name,
            metric,
            format='fixed',
            mode='w',
        )

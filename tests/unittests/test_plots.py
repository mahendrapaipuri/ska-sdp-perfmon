#!/usr/bin/env python3
""" This script does the unit tests for the dataframe related classes"""

import os
import pathlib
import unittest
import shutil
import pandas


from perfmon.common.plots import GenPlots


class TestPlots(unittest.TestCase):
    """Test plots class"""

    def setup(self):
        """Read data and make plots"""

        # Get project root
        project_root = pathlib.Path(__file__).parent.parent.parent

        # Get dir where test data is there
        data_file = os.path.join(
            project_root, 'tests', 'unittests', 'data', 'result',
            'metrics.h5',
        )

        # Plot dir
        plot_dir = os.path.join(os.getcwd(), 'plots')

        # Set data_dir and plot_dir
        self.config = {
            'plot_dir': plot_dir,
        }

        # Create plot_dir
        os.makedirs(self.config['plot_dir'], exist_ok=True)

        # Metric data as pandas df
        self.df_dict = {
            'cpu_metrics': pandas.read_hdf(data_file, 'cpu_metrics_3072'),
            'perf_metrics': pandas.read_hdf(data_file, 'perf_metrics_3072'),
        }

        # List of plots that would be generated
        self.plot_list = [
            'Average_CPU_Load_all_nodes.png', 'Average_Memory_all_nodes.png',
            'Average_Memory_Bandwidth_(Read)_all_nodes.png',
            'Total_Output_Traffic_all_nodes.png',
            'Total_Input_Traffic_all_nodes.png',
            'Total_Packets_Output_all_nodes.png',
            'Total_Packets_Input_all_nodes.png',
            'Total_Interconnect_Output_Traffic_all_nodes.png',
            'Total_Interconnect_Input_Traffic_all_nodes.png',
            'Total_Interconnect_Packets_Output_all_nodes.png',
            'Total_Interconnect_Packets_Input_all_nodes.png',
            'Total_RAPL_Package_Power_all_nodes.png',
            'Total_RAPL_DRAM_Power_all_nodes.png',
            'CPU_Load_per_node.png', 'Memory_per_node.png',
            'Memory_Bandwidth_(Read)_per_node.png',
            'Output_Traffic_per_node.png', 'Input_Traffic_per_node.png',
            'Packets_Output_per_node.png', 'Packets_Input_per_node.png',
            'Interconnect_Output_Traffic_per_node.png',
            'Interconnect_Input_Traffic_per_node.png',
            'Interconnect_Packets_Output_per_node.png',
            'Interconnect_Packets_Input_per_node.png',
            'RAPL_Package_Power_per_node.png', 'RAPL_DRAM_Power_per_node.png',
        ]

    def test_check_plots(self):
        """Test if plots are generated"""

        # Setup config
        self.setup()

        # Make plots
        make_plots = GenPlots(config=self.config, df_dict=self.df_dict)
        make_plots.go()

        # Plot dir
        plot_dir = self.config['plot_dir']

        # Check if plots are generated
        for plot_name in self.plot_list:
            assert os.path.isfile(os.path.join(plot_dir, plot_name))

        # Cleanup
        self.teardown()

    def teardown(self):
        """Remove plot_dir folder"""

        shutil.rmtree(self.config['plot_dir'])


if __name__ == '__main__':
    unittest.main(verbosity=2)

image: registry.gitlab.com/ska-telescope/sdp/ska-sdp-perfmon:slurm

stages:
  - pretest
  - unit-tests
  - functional-tests
  - coverage
  - pages

# GitLab project variables
variables:
  CI_API_V4_URL: "https://gitlab.com/api/v4"
  PROJECT_ID: "ska-telescope%2Fsdp%2Fska-sdp-perfmon"

lint:
  stage: pretest
  tags: [k8srunner]
  before_script:
    - pip install -r requirements.txt -r requirements-tests.txt
  script:
    - mkdir -p build
    - pylint --exit-zero perfmon > build/linting.xml
  artifacts:
    paths:
      - build/
    expire_in: 1 day

unittests:
  stage: unit-tests
  tags: [k8srunner]
  before_script:
    - cp .coveragerc build/
    - pip3 install -r requirements.txt -r requirements-tests.txt
    - export PYTHONPATH=$PWD
  script:
    - cd build
    - coverage run -m pytest ../tests/unittests/test_* --junitxml unit-tests.xml
  artifacts:
    paths:
      - build/
    expire_in: 1 day
  dependencies:
    - lint

test-monitor-metrics:
  stage: functional-tests
  tags: [k8srunner]
  before_script:
    - docker-entrypoint.sh
    - export USER=`whoami`  # Docker container does not set USER env variable by default
    - pip3 install -r requirements.txt -r requirements-tests.txt
    - export PYTHONPATH=$PWD
  script:
    - cd tests/
    - make test
    - cd ../build
    - sbatch ../tests/job-scripts/cpu-metrics-mpi-test.slurm
    - squeue && sleep 400 && squeue
    - cat slurm-2.out
    - cat $(ls | grep 'ska_sdp')
    - if [ ! -f ci-job/job-report-2.pdf ]; then exit 1; fi
  artifacts:
    paths:
      - build/
    expire_in: 1 day
  dependencies:
    - unittests

coverage:
  stage: coverage
  tags: [k8srunner]
  before_script:
    - pip3 install -r requirements.txt -r requirements-tests.txt
  script:
    - cd build
    - coverage html
    - coverage xml
  artifacts:
    paths:
      - build/
    expire_in: 1 day
  dependencies:
    - test-monitor-metrics

pages:
  stage: pages
  tags: [k8srunner]
  script:
    - cd build/
    - mkdir -p reports
    - mv unit-tests.xml reports/unit-tests.xml
    - mv code-coverage.xml reports/code-coverage.xml
    - mv linting.xml reports/linting.xml
    - mkdir -p ../public
    - cp code-coverage/index.html ../public/code-coverage.html
    - cp -r reports ../public
  artifacts:
    paths:
      - build/reports
      - public
#
# # Create Gitlab CI badges from CI metrics
# # https://developer.skatelescope.org/en/latest/tools/continuousintegration.html#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline
include:
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/post_step.yml'
